package com.application.myapplication

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment


abstract class BaseFragment : Fragment() {

    protected lateinit var mainActivity: MainActivity

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainActivity = activity as MainActivity
    }

    fun showProgress(show: Boolean) {
        mainActivity.progressView?.visibility = if (show) View.VISIBLE else View.GONE
    }

    fun goBack() {

        if (mainActivity.supportFragmentManager.backStackEntryCount > 0) {
            mainActivity.supportFragmentManager.popBackStackImmediate()
        }
    }
}
