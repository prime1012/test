package com.application.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.myapplication.DateUtils.getFormattedTimeChatLog
import com.application.myapplication.model.Chat
import com.application.myapplication.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.Item
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.chat_fragment.*
import kotlinx.android.synthetic.main.my_message_row.view.*
import kotlinx.android.synthetic.main.user_message_row.view.*
import org.jetbrains.anko.toast


class ChatFragment : BaseFragment(){

    val adapter = GroupAdapter<ViewHolder>()

    private lateinit var opponent: User
    private lateinit var currentUser: User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.chat_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        opponent = arguments?.getSerializable(ARG_ANOTHER_USER) as User
        toolbarTitle.text = opponent.name

        backBtn.setOnClickListener { goBack() }

        listenForMessages()

        val layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        layoutManager.stackFromEnd = true
        rvChat.layoutManager = layoutManager
        rvChat.hasFixedSize()
        rvChat.itemAnimator = DefaultItemAnimator()
        rvChat.layoutManager
        rvChat.adapter = adapter

        sendBtn.setOnClickListener {
            performSendMessage()
        }
    }

    private fun listenForMessages() {

        val fromId = FirebaseAuth.getInstance().uid ?: return
        val toId = opponent.uuid
        val ref = FirebaseDatabase.getInstance().getReference("/user-messages/$fromId/$toId")

        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                println(databaseError.message)
            }

            override fun onDataChange(dataSnapshot: DataSnapshot) {
                println(dataSnapshot.hasChildren())
                if (adapter.itemCount > 5){
                    rvChat?.smoothScrollToPosition(adapter.itemCount - 1)
                }
            }
        })

        ref.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(databaseError: DatabaseError) {
                showProgress(false)
            }

            override fun onChildMoved(dataSnapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildChanged(dataSnapshot: DataSnapshot, previousChildName: String?) {
            }

            override fun onChildAdded(dataSnapshot: DataSnapshot, previousChildName: String?) {

                dataSnapshot.getValue(Chat::class.java)?.let {

                    if (it.fromId == FirebaseAuth.getInstance().uid) {

                        val uid = FirebaseAuth.getInstance().uid ?: return
                        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")
                        ref.addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(databaseError: DatabaseError) {

                            }

                            override fun onDataChange(dataSnapshot: DataSnapshot) {
                                currentUser = dataSnapshot.getValue(User::class.java)!!

                                adapter.add(ChatFromItem(it.message, currentUser, it.timestamp))

                                if (adapter.itemCount > 5){
                                    rvChat?.smoothScrollToPosition(adapter.itemCount - 1)
                                }
                            }
                        })

                    } else {
                        adapter.add(ChatToItem(it.message, opponent, it.timestamp))

                        if (adapter.itemCount > 5){
                            rvChat?.smoothScrollToPosition(adapter.itemCount - 1)
                        }
                    }
                }

                showProgress(false)
            }

            override fun onChildRemoved(dataSnapshot: DataSnapshot) {
                showProgress(false)
            }

        })

    }

    private fun performSendMessage() {

        val text = textMessage.text.toString().trim()
        if (text.isEmpty()) {
            view?.context?.toast("Сначала напишите что-то!")
            return
        }

        val fromId = FirebaseAuth.getInstance().uid ?: return
        val toId = opponent.uuid

        val reference = FirebaseDatabase.getInstance().getReference("/user-messages/$fromId/$toId").push()
        val toReference = FirebaseDatabase.getInstance().getReference("/user-messages/$toId/$fromId").push()

        val chatMessage = Chat(reference.key!!, text, fromId, toId, System.currentTimeMillis() / 1000)
        reference.setValue(chatMessage)
            .addOnSuccessListener {

                textMessage.text.clear()

                if (adapter.itemCount > 5){
                    rvChat?.smoothScrollToPosition(adapter.itemCount - 1)
                }
            }

        toReference.setValue(chatMessage)

    }

    companion object {

        val ARG_ANOTHER_USER = "arg_user"

        fun newInstance(user: User): ChatFragment {
            val args = Bundle()
            args.putSerializable(ARG_ANOTHER_USER, user)
            val fragment = ChatFragment()
            fragment.arguments = args
            return fragment
        }
    }
}

class ChatFromItem(val text: String, val user: User, val timestamp: Long) : Item<ViewHolder>() {

    override fun bind(viewHolder: ViewHolder, position: Int) {

        viewHolder.itemView.text.text = text
        viewHolder.itemView.time.text = getFormattedTimeChatLog(timestamp)
    }

    override fun getLayout(): Int {
        return R.layout.my_message_row
    }

}

class ChatToItem(val text: String, val user: User, val timestamp: Long) : Item<ViewHolder>() {

    override fun bind(viewHolder: ViewHolder, position: Int) {
        viewHolder.itemView.text2.text = text
        viewHolder.itemView.time2.text = getFormattedTimeChatLog(timestamp)
    }

    override fun getLayout(): Int {
        return R.layout.user_message_row
    }

}
