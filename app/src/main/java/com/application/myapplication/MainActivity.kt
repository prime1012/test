package com.application.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.pixplicity.easyprefs.library.Prefs
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    var progressView: FrameLayout? = null

    private var doubleBackToExitPressedOnce: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        progressView = findViewById(R.id.progressMain)

        if (savedInstanceState == null){

            if (Prefs.getString("my_uuid", null) != null){
                hardReplaceFragment(MainFragment())
            } else {
                hardReplaceFragment(SignUpFragment())
            }

        }
    }

    override fun onBackPressed() {

        if (supportFragmentManager.backStackEntryCount != 0) {
            super.onBackPressed()
            return
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            finish()
            return
        }

        this.doubleBackToExitPressedOnce = true
        toast("Для выхода, \nнажмите BACK еще раз")

        Handler().postDelayed({ doubleBackToExitPressedOnce = false }, 2000)
    }

    fun hardReplaceFragment(fragment: Fragment) {
        supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        supportFragmentManager.beginTransaction()
            .setCustomAnimations(R.anim.fade_in, R.anim.fade_out)
            .replace(R.id.placeholder, fragment)
            .commitAllowingStateLoss()
    }
}
