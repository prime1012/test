package com.application.myapplication

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.application.myapplication.adapter.UsersAdapter
import com.application.myapplication.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.sign_in_fragment.*
import org.jetbrains.anko.toast


class MainFragment : BaseFragment(){

    private lateinit var adapter: UsersAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getAllUsers()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = UsersAdapter{
            val fragment = ChatFragment.newInstance(it)
            mainActivity.supportFragmentManager
                .beginTransaction()
                .hide(this)
                .add(R.id.placeholder, fragment)
                .show(fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss()
        }
        rvUsers.layoutManager = LinearLayoutManager(view.context, RecyclerView.VERTICAL, false)
        rvUsers.hasFixedSize()
        rvUsers.itemAnimator = DefaultItemAnimator()
        rvUsers.adapter = adapter

        swipeRefreshLayout.setOnRefreshListener {
            getAllUsers()
        }

        logoutBtn.setOnClickListener {

            val builder = AlertDialog.Builder(view.context)

            builder.let {
                it.setTitle("Смена пользователя")
                it.setMessage("Вы точно хотите сменить пользователя?")
                it.setPositiveButton("Да"){ dialog, _ ->

                    dialog.dismiss()
                    Prefs.clear()
                    mainActivity.hardReplaceFragment(SignInFragment())
                }
                it.setNegativeButton("Нет"){ dialog, _ ->
                    dialog.dismiss()
                }
            }

            val dialog: AlertDialog = builder.create()
            dialog.show()


        }
    }

    private fun getAllUsers(){

        swipeRefreshLayout.isRefreshing = true

        val ref = FirebaseDatabase.getInstance().getReference("/users")
        ref.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {

                val list = arrayListOf<User>()
                val dataSnapshots = dataSnapshot.children.iterator()
                while (dataSnapshots.hasNext()) {
                    val dataSnapshotChild = dataSnapshots.next()
                    val user = dataSnapshotChild.getValue(User::class.java)
                    if (user?.uuid != Prefs.getString("my_uuid", null)){
                        list.add(user!!)
                    }
                }

                adapter.setList(list)

                swipeRefreshLayout.isRefreshing = false
            }

            override fun onCancelled(databaseError: DatabaseError) {
                view?.context?.toast(databaseError.message)
                swipeRefreshLayout.isRefreshing = false
            }
        })

    }
}
