package com.application.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.sign_in_fragment.*
import org.jetbrains.anko.toast


class SignInFragment : BaseFragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sign_in_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signUpBtn.setOnClickListener {
            mainActivity.hardReplaceFragment(SignUpFragment())
        }

        btnContinue.setOnClickListener {
            performLogin()
        }
    }

    private fun performLogin() {

        val email = editEmail.text.toString().trim()
        val password = editPassword.text.toString().trim()

        if (email.isEmpty()){
            view?.context?.toast("Введите ваш email!")
            return
        }

        if (password.isEmpty()){
            view?.context?.toast("Введите пароль!")
            return
        }

        showProgress(true)

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, password)
            .addOnCompleteListener {

                if (!it.isSuccessful) return@addOnCompleteListener

                val uid = FirebaseAuth.getInstance().uid
                Prefs.putString("my_uuid", uid)
                showProgress(false)

                mainActivity.hardReplaceFragment(MainFragment())

            }
            .addOnFailureListener {
                showProgress(false)
                view?.context?.toast("${it.message}")
            }
    }
}
