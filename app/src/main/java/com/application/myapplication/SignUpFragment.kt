package com.application.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.application.myapplication.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.pixplicity.easyprefs.library.Prefs
import kotlinx.android.synthetic.main.sign_up_fragment.*
import org.jetbrains.anko.toast


class SignUpFragment : BaseFragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.sign_up_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signInBtn.setOnClickListener {
            mainActivity.hardReplaceFragment(SignInFragment())
        }

        btnContinue.setOnClickListener {
            performRegistration()
        }
    }

    private fun performRegistration() {

        val email = editEmail.text.toString().trim()
        val password = editPassword.text.toString().trim()
        val name = editName.text.toString().trim()

        if (email.isEmpty()){
            view?.context?.toast("Введите email!")
            return
        }

        if (password.isEmpty()){
            view?.context?.toast("Введите пароль!")
            return
        }

        if (name.isEmpty()){
            view?.context?.toast("Введите имя!")
            return
        }

        showProgress(true)

        FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {
                if (!it.isSuccessful) return@addOnCompleteListener

                saveUserToFirebaseDatabase()
            }
            .addOnFailureListener {
                view?.context?.toast("${it.message}")
                showProgress(false)
            }
    }


    private fun saveUserToFirebaseDatabase() {

        val uid = FirebaseAuth.getInstance().uid ?: return
        val ref = FirebaseDatabase.getInstance().getReference("/users/$uid")

        val user = User(uid, editName.text.toString().trim(), editEmail.text.toString().trim())

        ref.setValue(user)
            .addOnSuccessListener {

                Prefs.putString("my_uuid", uid)
                showProgress(false)
                mainActivity.hardReplaceFragment(MainFragment())

            }
            .addOnFailureListener {
                showProgress(false)
                view?.context?.toast("${it.message}")
            }
    }
}
