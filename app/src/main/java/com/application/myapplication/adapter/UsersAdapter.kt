package com.application.myapplication.adapter


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.application.myapplication.R
import com.application.myapplication.model.User
import org.jetbrains.anko.find

class UsersAdapter(private val callback: ((obj: User) -> Unit)? = null) : RecyclerView.Adapter<UsersAdapter.MyViewHolder>() {

    private var userList: List<User> = listOf()

    fun setList(list: List<User>) {
        userList = list
        notifyDataSetChanged()
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var name: TextView = view.find(R.id.name)
        var email: TextView = view.find(R.id.email)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_row, parent, false))
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val user = userList[position]

        user.name.let { holder.name.text = it }

        user.email.let { holder.email.text = it }

        holder.itemView.setOnClickListener {
            callback?.invoke(user)
        }
    }

    override fun getItemCount(): Int {
        return  userList.size
    }
}
