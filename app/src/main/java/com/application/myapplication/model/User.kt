package com.application.myapplication.model

import java.io.Serializable

class User(var uuid: String = "", var name: String = "", var email: String = "") : Serializable
